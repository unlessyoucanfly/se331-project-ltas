import {Component, OnInit} from '@angular/core';
import {StudentService} from '../service/student.service';
import {Router} from '@angular/router';
import {ActivityService} from '../service/activity.service';
import {TeacherService} from '../service/teacher.service';
import {Location} from '@angular/common';
import {Activity} from '../model';
import moment from 'moment';
import matchSorter from 'match-sorter';

@Component({
  selector: 'app-student-activity-enrolled',
  templateUrl: './student-activity-enrolled.component.html',
  styleUrls: ['./student-activity-enrolled.component.scss']
})
export class StudentActivityEnrolledComponent implements OnInit {
  constructor(
    private studentService: StudentService,
    private router: Router,
    private activityService: ActivityService,
    private teacherService: TeacherService,
    private location: Location,
  ) {
    this.activities = this.activityService.activitiesByStudentId(this.studentService.loginStudent!.id);
  }

  matchSorter = matchSorter;

  filter = '';

  beginTime = '';
  endTime = '';

  activities: Activity[] = [];

  ngOnInit() {
    if (!this.studentService.loginStudent) {
      this.router.navigateByUrl('/student/login');
    }
  }


  onInputFilter(value: string) {
    this.filter = value.trim();
    this.activities = matchSorter(
      this.activityService.activitiesByStudentId(this.studentService.loginStudent!.id),
      this.filter,
      {keys: ['name']}
    );
    this.filterByTimeRange();
  }

  onInputBeginTime(value: string) {
    console.log('onInputBeginTime', value);
    this.beginTime = moment(value.trim()).format('YYYY-MM-DD');
    this.filterByTimeRange();
  }

  onInputEndTime(value: string) {
    console.log('onInputEndTime', value);
    this.endTime = moment(value.trim()).format('YYYY-MM-DD');
    this.filterByTimeRange();
  }

  filterByTimeRange() {
    const b = this.beginTime;
    const e = this.endTime;
    let bt: moment.Moment, et: moment.Moment;
    try {
      if (b) {
        bt = moment(b);
      } else {
        bt = moment('1900-01-01');
      }
    } catch (e) {
      bt = moment('1900-01-01');
    }
    try {
      if (e) {
        et = moment(e + ' 23:59');
      } else {
        et = moment('2900-01-01');
      }
    } catch (e) {
      et = moment('2900-01-01');
    }
    console.log(bt.format(), et.format());
    this.activities = matchSorter(
      this.activityService.activitiesByStudentId(this.studentService.loginStudent!.id),
      this.filter,
      {keys: ['name']}
    );
    this.activities = this.activities.filter(x => {
      return (
        bt.toDate().getTime() <=
        moment(x.datetime)
          .toDate()
          .getTime() &&
        moment(x.datetime)
          .toDate()
          .getTime() <= et.toDate().getTime()
      );
    });
  }

  apply(activity: Activity) {
    if (window.confirm(`Are you sure to apply for this activity?`)) {
      this.activityService.apply(activity, this.studentService.loginStudent!);
    }
  }
}
