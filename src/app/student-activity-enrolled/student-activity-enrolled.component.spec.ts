import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {StudentActivityEnrolledComponent} from './student-activity-enrolled.component';

describe('StudentActivityEnrolledComponent', () => {
  let component: StudentActivityEnrolledComponent;
  let fixture: ComponentFixture<StudentActivityEnrolledComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StudentActivityEnrolledComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentActivityEnrolledComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
